A triple-network organization for the mouse brain
================
Joanes Grandjean

## Summary

In this section, we perform a seed-based analysis on viral tracer data
downloaded from the AIBS. We further compare this to resting-state fMRI
and human DTI.

### Inputs

Viral tracer maps Resting-state mouse datasets scans seeds (see
[asset](assest.md))

### Outputs

**Figure 2, S5A, S6ACE**  
Seed-based maps from viral
tracer(assets/SBA/tracer/thresh\_tacer\_\*.nii.gz)

**Figure S6BDF**  
Seed-based maps from viral
tracer(assets/SBA/fMRI/thresh\_tacer\_\*.nii.gz)

**Figure S5BC**  
HCP 7T probtrax group summary projection as a function of DK atlas
(assets/wm/DTI*.txt) Synthetic tracer data humanized as a function of DK
atlas (assets/wm/tracer*.txt)

## Detailed code

### Loading the R main variables

``` r
library('stringr')
library('ggplot2')
require('svglite')
library('nat')
library('oro.nifti')
library('reshape2')
library('extrafont')
library('multcomp')
library('parameters')
library('effectsize')
library('reshape2')

#set the same variables as for bash_env.sh

init_folder <- "/home/traaffneu/joagra/code/insula"
data_folder <- "/project/4180000.18"

study_folder <- paste(data_folder, "/rest", sep='')
roi_folder <- paste(data_folder, "/SBA_roi", sep='')
SBA.folder <- paste(study_folder, '/SBA/', sep = '')
viral.folder <- paste(study_folder, "tracer", sep='')

template_mask <- "assets/template/mask_200um.nii"
template <- "assets/template/ABI_template_200um.nii"
atlas <- "assets/template/ABI_atlas_reduced_TIDO_200um.nii.gz"
atlas.label <- "assets/template/ABI_atlas_reduced_TIDO.csv"

study <- read.csv(paste(study_folder,'/participants.tsv',sep='')
study <- study[!study$dataset %in% c('aes1', 'MEPSI', 'AD1', 'ME1'), ]
```

## Mouse seed-based analysis

### Viral tracer analysis

Firstly, we download and convert to NIFTI format all viral tracer maps
from wild-type animals. This corresponds to \~500 maps.

### Figure 2, Figure S5, S6

The maps are concatenated, and seed-based analysis is performed on the
concatenated maps.

``` bash
source bash_env.sh

cd $viral_folder

sform_template=$(fslorient -getsform /groupshare/traaffneu/preclinimg/templates/my_templates/ABI_template_50um.nii)
cd nifti
ls . | while read line
do
3dresample -inset $line -prefix tmp.nii.gz
fslorient -setsform $sform_template tmp.nii.gz
fslorient -setqform $sform_template tmp.nii.gz
3dresample -inset tmp.nii.gz -prefix 'rs_'$line -master /groupshare/traaffneu/preclinimg/templates/my_templates/ABI_template_50um.nii
rm tmp.nii.gz
done
cd ..
echo "cd "${PWD} > ${PWD}/script/merge.sh
echo "fslmerge -t tracer_merge nifti/rs_*" >> ${PWD}/script/merge.sh
chmod +x ${PWD}/script/merge.sh
qsub -l 'procs=1,mem=32gb,walltime=00:15:00' ${PWD}/script/merge.sh

mkdir -p SBA
mkdir -p script
counter=0

ls $roi_folder_50um/*.nii.gz | while read SB
do

qscript='SBA_job_'$counter'.sh'

SB_base=$(basename $SB)
SB_noext="$(remove_ext $SB_base)"


echo "cd "${PWD} > ${PWD}/script/$qscript
echo "fslmeants -i tracer_merge.nii.gz -o 'SBA/tracer_'${SB_noext}'.txt' -m ${SB}" >> ${PWD}/script/$qscript

echo "fsl_glm -i tracer_merge.nii.gz -d 'SBA/tracer_'${SB_noext}'.txt' -m ${template_mask_50}  --out_z='SBA/tracer_'${SB_noext}'.nii.gz' --demean" >> ${PWD}/script/$qscript

chmod +x ${PWD}/script/$qscript
#because we have access to a cluster system, we submit jobs. 
qsub -l 'procs=1,mem=64gb,walltime=00:15:00' ${PWD}/script/$qscript
#if a cluster is not available, comment the line above and use the following instead. Might be a tad slow to run. 
#bash ${PWD}/script/$qscript


counter=$((counter+1))
done
rm -r script/SBA_job*


easythresh SBA/tracer_aAI.nii.gz ${template_mask_50} 1.92 0.05 ${template_50} tacer_aAI_export

easythresh SBA/tracer_ACA.nii.gz ${template_mask_50} 1.92 0.05 ${template_50} tacer_ACA_export

easythresh SBA/tracer_MOp.nii.gz ${template_mask_50} 1.92 0.05 ${template_50} tacer_MOp_export

fslmeants -i SBA/tracer_aAI.nii.gz -o $template_mask_50 -o $init_folder'/assets/img/SBA_tracer_aAI_z' --label=$atlas_50
fslmeants -i SBA/tracer_ACA.nii.gz -o $template_mask_50 -o $init_folder'/assets/img/SBA_tracer_ACA_z' --label=$atlas_50
fslmeants -i SBA/tracer_MOp.nii.gz -o $template_mask_50 -o $init_folder'/assets/img/SBA_tracer_MOp_z' --label=$atlas_50
```

``` bash
source bash_env.sh

cp $viral_folder/rendered_thresh_tacer_aAI_export.png assets/img/SBA_mouse_tracer_aAI.png
cp $viral_folder/rendered_thresh_tacer_MOp_export.png assets/img/SBA_mouse_tracer_MOp.png
cp $viral_folder/rendered_thresh_tacer_ACA_export.png assets/img/SBA_mouse_tracer_ACA.png
```

![tracer-based anterior insular area SBA in the
mouse](assets/img/SBA_mouse_tracer_aAI.png) ![tracer-based anterior
insular area SBA in the mouse](assets/img/SBA_mouse_tracer_ACA.png)
![tracer-based anterior insular area SBA in the
mouse](assets/img/SBA_mouse_tracer_MOp.png)

### Figure S6

First let’s apply smoothing to a previously pre-processed rsfMRI dataset
and estimate single subject mouse resting-state fMRI

``` bash
source bash_env.sh

#cp -r  $init_folder $study_folder
cd $study_folder

mkdir -p SBA
mkdir -p script
mkdir -p data_smooth
counter=0

#first we apply minial smoothing of 0.2mm to help with gradient analysis
ls data | while read scan
do
scan_noext="$(remove_ext $scan)"
echo "now doing scan "$scan
qscript='SBA_job_'$counter'.sh'

echo "cd "${PWD} > ${PWD}/script/$qscript
echo "3dBlurInMask -input data/$scan -prefix data_smooth/$scan -mask $template_mask -FWHM 0.2" >> ${PWD}/script/$qscript

chmod +x ${PWD}/script/$qscript

#because we have access to a cluster system, we submit jobs. 
qsub -l 'procs=1,mem=8gb,walltime=00:10:00' ${PWD}/script/$qscript
#if a cluster is not available, comment the line above and use the following instead. Might be a tad slow to run. 
#bash ${PWD}/script/$qscript


counter=$((counter+1))
done
rm script/SBA_job*


counter=0


#loop through the processed scans, for each scans, loop through the region-of-interests and estimate seed-based analysis
ls data_smooth | while read scan
do
scan_noext="$(remove_ext $scan)"
echo "now doing scan "$scan
qscript='SBA_job_'$counter'.sh'

ls $roi_folder/*.nii.gz | while read SB
do
SB_base=$(basename $SB)
SB_noext="$(remove_ext $SB_base)"


echo "cd "${PWD} > ${PWD}/script/$qscript
echo "fslmeants -i data_smooth/$scan -o SBA/${scan_noext}'_'${SB_noext}'.txt' -m ${SB}" >> ${PWD}/script/$qscript

echo "fsl_glm -i data_smooth/$scan -d SBA/${scan_noext}'_'${SB_noext}'.txt' -m ${template_mask}  --out_z=SBA/${scan_noext}'_'${SB_noext}'.nii.gz' --demean" >> ${PWD}/script/$qscript

chmod +x ${PWD}/script/$qscript
#because we have access to a cluster system, we submit jobs. 
qsub -l 'procs=1,mem=8gb,walltime=00:10:00' ${PWD}/script/$qscript
#if a cluster is not available, comment the line above and use the following instead. Might be a tad slow to run. 
#bash ${PWD}/script/$qscript


counter=$((counter+1))
done
done
rm script/SBA_*
```

Let’s now estimate group level SBA, one sample t-test as well as group
comparisons. The script below generates a design matrix and bash script
to perform a FSL randomize between-group comparison.

``` r
study.ctl <- study[study$mediso_ctl == 'yes', ]
study.ctl$condition <- droplevels(study.ctl$condition)
#levels(study.ctl$condition)

roi.list <- c('aAI', 'MOp', 'ACA')
for (i in 1:length(roi.list)) {
  export_folder <- paste(study_folder, '/SBA_group/', roi.list[i], sep = '')
  dir.create(export_folder,
             showWarnings = F,
             recursive = T)
  
  ID_factor <- levels(factor(study.ctl$ID))
  Group_factor <- levels(factor(study.ctl$condition))
  ID_design <- c()
  Group_design <- c()
  ID_group <- c()
  file_list <- c()
  
  
  for (w in 1:dim(study.ctl)[1])
  {
    file_list[w] <-
      file.path(SBA.folder,
                paste(
                  str_pad(study.ctl$ID[w], 4, pad = "0"),
                  '_',
                  roi.list[i],
                  '.nii.gz',
                  sep = ''
                ))
    ID_vec <- rep(0, length(ID_factor))
    ID_vec[which(ID_factor == study.ctl$ID[w])] <- 1
    ID_design <- rbind(ID_design, ID_vec)
    ID_group <- c(ID_group, which(ID_factor == study.ctl$ID[w]))
    
    #Group_vec<-rep(0,length(Group_factor))
    #Group_vec[which(Group_factor==study.ctl$condition[w])]<-1
    # Group_design<-rbind(Group_design,Group_vec)
    Group_design <- rbind(Group_design, 1)
    
    
  }
  
  final_design <- cbind(Group_design)
  write.table(
    file_list,
    paste(export_folder, '/', 'scan_list.txt', sep = ''),
    row.names = F,
    col.names = F,
    quote = F
  )
  #head(final_design)
  
  
  
  K <- rbind(c(1),
             c(-1))
  
  
  con_name <- c("ctl+", "ctl-")
  
  desgin_mat <- c()
  ii <- 1
  desgin_mat[ii] <- paste('/NumWaves',  dim(final_design)[2], sep = "\t")
  ii <- ii + 1
  desgin_mat[ii] <- paste('/NumPoints',  dim(final_design)[1], sep = "\t")
  ii <- ii + 1
  desgin_mat[ii] <-
    paste('/PPheights\t', paste(rep('1', dim(final_design)[1]), collapse = '\t'), sep =
            "\t")
  ii <- ii + 1
  desgin_mat[ii] <- ''
  ii <- ii + 1
  desgin_mat[ii] <- paste('/Matrix', sep = "\t")
  for (i in 1:dim(final_design)[1]) {
    ii <- ii + 1
    desgin_mat[ii] <-
      paste(as.character(final_design[i, ]), collapse = '\t')
    
  }
  write.table(
    desgin_mat,
    file.path(export_folder, 'design.mat'),
    quote = F,
    row.names = F,
    col.names = F
  )
  
  
  desgin_con <- c()
  ii <- 1
  for (i in 1:dim(K)[1]) {
    desgin_con[ii] <-
      paste(paste('/ContrastName', i, sep = ''),  con_name[i], sep = "\t")
    ii <- ii + 1
  }
  desgin_con[ii] <- paste('/NumWaves',  dim(K)[2], sep = "\t")
  ii <- ii + 1
  desgin_con[ii] <- paste('/NumContrasts',  dim(K)[1], sep = "\t")
  ii <- ii + 1
  desgin_con[ii] <-
    paste('/PPheights\t', paste(rep('1', dim(K)[1]), collapse = '\t'), sep =
            "\t")
  ii <- ii + 1
  desgin_con[ii] <-
    paste('/RequiredEffect\t', paste(rep('1', dim(K)[1]), collapse = '\t'), sep =
            "\t")
  ii <- ii + 1
  desgin_con[ii] <- ''
  ii <- ii + 1
  desgin_con[ii] <- paste('/Matrix', sep = "\t")
  for (i in 1:dim(K)[1]) {
    ii <- ii + 1
    desgin_con[ii] <- paste(as.character(K[i, ]), collapse = '\t')
    
  }
  write.table(
    desgin_con,
    file.path(export_folder, 'design.con'),
    quote = F,
    row.names = F,
    col.names = F
  )
  
  
  ii <- 1
  script.randomize <- c()
  script.randomize[ii] <- paste('#!/bin/bash', sep = "\t")
  ii <- ii + 1
  script.randomize[ii] <- paste('cd', export_folder, sep = ' ')
  ii <- ii + 1
  script.randomize[ii] <-
    paste('fslmerge -t merge $(cat scan_list.txt)', sep = "\t")
  ii <- ii + 1
  script.randomize[ii] <-
    paste(
      'fsl_glm -i merge.nii.gz --out_z=glm_z --out_p=glm_p -d design.mat -c design.con -m',
      template_mask,
      sep = " "
    )
  ii <- ii + 1
  script.randomize[ii] <- paste('fslsplit glm_z', sep = " ")
  ii <- ii + 1
  script.randomize[ii] <-
    paste('easythresh vol0000.nii.gz',
          template_mask,
          '4.42 0.001',
          template ,
          'thr_pos',
          sep = " ")
  ii <- ii + 1
  script.randomize[ii] <-
    paste('easythresh vol0001.nii.gz',
          template_mask,
          '4.42 0.001',
          template ,
          'thr_neg',
          sep = " ")
  ii <- ii + 1
  script.randomize[ii] <-
    paste(
      'fslmaths thresh_thr_neg.nii.gz -mul -1 -add thresh_thr_pos.nii.gz thresh_thr.nii.gz',
      sep = " "
    )
  
  
  
  
  #file named randomize because I originally intended to use non-parametric stats, but this turned out impractical for other anlaysis
  write.table(
    script.randomize,
    file.path(export_folder, 'randomize.sh'),
    quote = F,
    row.names = F,
    col.names = F
  )
}
```

We can now run it in bash using the following.

``` bash
source bash_env.sh

cd $study_folder'/SBA_group/aAI'
chmod +x ${PWD}/randomize.sh

#because we have access to a cluster system, we submit jobs. 
qsub -l 'procs=1,mem=16gb,walltime=04:00:00' ${PWD}/randomize.sh
#if a cluster is not available, comment the line above and use the following instead. Might be a tad slow to run. 
#bash ${PWD}/randomize.sh
fslmeants -i glm_z.nii.gz -o $template_mask -o $init_folder'/assets/img/SBA_aAI_z' --label=$atlas

cd $study_folder'/SBA_group/MOp'
chmod +x ${PWD}/randomize.sh

#because we have access to a cluster system, we submit jobs. 
qsub -l 'procs=1,mem=16gb,walltime=04:00:00' ${PWD}/randomize.sh
#if a cluster is not available, comment the line above and use the following instead. Might be a tad slow to run. 
#bash ${PWD}/randomize.sh
fslmeants -i glm_z.nii.gz -o $template_mask -o $init_folder'/assets/img/SBA_MOp_z' --label=$atlas

cd $study_folder'/SBA_group/ACA'
chmod +x ${PWD}/randomize.sh

#because we have access to a cluster system, we submit jobs. 
qsub -l 'procs=1,mem=16gb,walltime=04:00:00' ${PWD}/randomize.sh
#if a cluster is not available, comment the line above and use the following instead. Might be a tad slow to run. 
#bash ${PWD}/randomize.sh
fslmeants -i glm_z.nii.gz -o $template_mask -o $init_folder'/assets/img/SBA_ACA_z' --label=$atlas
```

``` bash
source bash_env.sh

cp /project/4180000.18/rest_ins/SBA_group/aAI/rendered_thresh_thr_pos.png assets/img/SBA_mouse_aAI.png
cp /project/4180000.18/rest_ins/SBA_group/MOp/rendered_thresh_thr_pos.png assets/img/SBA_mouse_MOp.png
cp /project/4180000.18/rest_ins/SBA_group/ACA/rendered_thresh_thr_pos.png assets/img/SBA_mouse_ACA.png
```

![anterior insular area SBA in the mouse](assets/img/SBA_mouse_aAI.png)
Seed-based analysis of the mouse anterior insular area, derived from 290
rsfMRI scans.

![anterior cingulate area SBA in the
mouse](assets/img/SBA_mouse_ACA.png) Seed-based analysis of the mouse
anterior cingulate area, derived from 290 rsfMRI scans.

![Primary motor area SBA in the mouse](assets/img/SBA_mouse_MOp.png)
Seed-based analysis of the mouse primary motor area, derived from 290
rsfMRI scans.

``` r
library(ggplot2)
library(gridExtra)
library(parameters)

DTI.AI <-
  c(t(read.table(
    'assets/wm/DTI_aAI_dk_meants.txt', header = F
  )))
DTI.ACA <-
  c(t(read.table(
    'assets/wm/DTI_ACA_dk_meants.txt', header = F
  )))
DTI.dlpfc <-
  c(t(read.table(
    'assets/wm/DTI_dlpfc_dk_meants.txt', header = F
  )))

tracer.AI <-
  c(t(
    read.table('assets/wm/tracer_aAI_human_meants.txt', header = F)
  ))
tracer.ACA <-
  c(t(
    read.table('assets/wm/tracer_ACA_human_meants.txt', header = F)
  ))
tracer.MOp <-
  c(t(
    read.table('assets/wm/tracer_MOp_human_meants.txt', header = F)
  ))

roi <- rep(1:length(DTI.AI), 3)
seed <-
  c(rep('ACA', length(DTI.AI)), rep('aAI', length(DTI.ACA)), rep('dlpfc', length(DTI.dlpfc)))
df <-
  data.frame(roi,
             seed,
             c(DTI.ACA, DTI.AI, DTI.dlpfc),
             c(tracer.ACA, tracer.AI, tracer.MOp))
names(df) <- c('roiID', 'seed', 'DTI', 'synthetic')

p1<- qplot(DTI,synthetic,data=df[df$seed=='ACA',]) +   geom_smooth(method = "lm", formula = y ~ x, color='#e41a1c',fill='#e41a1c') + theme_classic() + labs(y ='Synthetic [a.u.]', x = 'Fibre [z-score]')+theme(text = element_text(size=12))
p2<- qplot(DTI,synthetic,data=df[df$seed=='aAI',]) +   geom_smooth(method = "lm", formula = y ~ x, color='#4daf4a',fill='#4daf4a') + theme_classic() + labs(y ='Synthetic [a.u.]', x = 'Fibre [z-score]')+theme(text = element_text(size=12))
p3<- qplot(DTI,synthetic,data=df[df$seed=='dlpfc',]) +   geom_smooth(method = "lm", formula = y ~ x, color='#377eb8',fill='#377eb8') + theme_classic() + labs(y ='Synthetic [a.u.]', x = 'Fibre [z-score]')+theme(text = element_text(size=12))

p4<-grid.arrange(p1, p2, p3, nrow = 2)

ggsave(
  'assets/img/DTI2tracer.png',
  plot = p4,
  device = 'png',
  units = 'mm',
  dpi = 300,
  width = 166,
  height = 120
)
ggsave(
  'assets/img/DTI2tracer.svg',
  plot = p4,
  device = 'svg',
  units = 'mm',
  dpi = 300,
  width = 166,
  height = 120
)

mod <- lm(synthetic ~ DTI * seed, df)
s <- parameters(mod,summary  = TRUE, standardize = 'refit')
capture.output(s, file = "assets/wm/DTI_synthetic.txt")
s
```

![Figure S5b](assets/img/DTI2tracer.svg)
