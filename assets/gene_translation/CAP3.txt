Parameter   | Coefficient |   SE |        95% CI |    t(81) |      p
--------------------------------------------------------------------
(Intercept) |    2.75e-17 | 0.09 | [-0.18, 0.18] | 3.01e-16 | > .999
true        |        0.56 | 0.09 | [ 0.38, 0.75] |     6.15 | < .001

Model: synthetic ~ true (83 Observations)
Residual standard deviation: 0.831 (df = 81)
R2: 0.318; adjusted R2: 0.310
