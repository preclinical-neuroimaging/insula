A triple-network organization for the mouse brain
================
Joanes Grandjean

### Table of content

1.  [Introduction](#introduction)  
2.  [environements](#environements)  
3.  [Asset preparation](asset.md)  
4.  [Co-activation patterns analysis](cap.md)  
5.  [Structural and functional connectivity](connectivity.md)  
6.  [Optogenetics](opto.md)

### Introduction

In this study, we establish the presence of a triple network system in
the mouse brain. We use a combination of publicly available mouse
functional MRI data, together with human and macaque data. This page and
the following demonstrate the analysis pipeline and the figure output
generated. The repository also includes statistical maps and tables to
reproduce the figures.

Please raise an issue or contact the corresponding author for additional
information.

### environements

The following scripts were run using the following environments.  
R version 3.5.1 (Feather Spray)  
rstudio 1.4.1717  
FSL 6.0.0  
AFNI AFNI\_16.1.15  
ANTS 2.1.0.post370-ga466e

and the following r packages (see sessionInfo.txt for additional
details)  
extrafont==0.17  
reshape2==1.4.3  
oro.nifti==0.10.1  
nat==1.8.14  
rgl==0.100.50  
ggplot==2\_3.1.0  
stringr==1.4.0  
multcomp==1.4-10 effectsize==0.4.4-1 parameters==0.13.0

In addition, for plots, we used MRIcro\_gl 1.2
(<https://github.com/rordenlab/MRIcroGL12>) and ImageMagick 6.9.10-23
Q16 x86\_64 20190101 <https://imagemagick.org>.

### Setting up R environment

To download the same R package environment, we use
[packrat](https://rstudio.github.io/packrat/)

``` r
#install packrat
install.packages("packrat")

#restore the packages installed under packrat. This will install packages in your environment. 
packrat::restore()

#writing the sessionInfo for good measure
writeLines(capture.output(sessionInfo()), "sessionInfo.txt")
```
